#
# https://learn.adafruit.com/adafruit-4-channel-adc-breakouts/python-circuitpython
# https://www.instructables.com/id/16-bit-I2C-Temperature-Monitor-Using-Arduino/
# https://learn.adafruit.com/thermistor/circuitpython\
# https://learn.adafruit.com/thermistor/using-a-thermistor#self-heating-3-22
#
import math
import time
import board
import busio

i2c = busio.I2C(board.SCL, board.SDA)

import adafruit_ads1x15.ads1115 as ADS

from adafruit_ads1x15.analog_in import AnalogIn

ads = ADS.ADS1115(i2c)

therm0 = AnalogIn(ads, ADS.P0)   # voltage divider/thermistor input
therm1 = AnalogIn(ads, ADS.P1)   # voltage divider/thermistor input
therm2 = AnalogIn(ads, ADS.P2)   # voltage divider/thermistor input
therm3 = AnalogIn(ads, ADS.P3)   # voltage divider/thermistor input
refVolts = 5 #AnalogIn(ads, ADS.P0)   # connect to +3.3v as a reference to the max value
refR0 = 9850       # resistor to compare thermistor to
refR1 = 9850       # resistor to compare thermistor to
refR2 = 9850       # resistor to compare thermistor to
refR3 = 9850       # resistor to compare thermistor to

def steinhart_temperature_C(r, Ro=10000.0, To=25.0, beta=3950.0):
    steinhart = math.log(r / Ro) / beta      # log(R/Ro) / beta
    steinhart += 1.0 / (To + 273.15)         # log(R/Ro) / beta + 1/To
    steinhart = (1.0 / steinhart) - 273.15   # Invert, convert to C
    return steinhart

def output_temp(thermistor, refR) :
    #print ('thermistor voltage: %.2f' % (thermistor.voltage), end='\n')
    #print ('thermistor: %.2f refVolts: %.2f' % (thermistor.value, refVolts), end='\n')
    R = refR / (refVolts/thermistor.voltage - 1)
    #print('R: %.2f ohms' % (R), end='\n')
    celcius = steinhart_temperature_C(R, refR)
    farenheit = 1.8 * celcius +32
    return farenheit
    #print('\r%.2f°C %.2f°F' % (celcius, farenheit), sep=' ', end='', flush=True)
    #print('\n')

while True:
    print('\r%.2f %.2f %.2f' %
          (output_temp(therm0, refR0),
           output_temp(therm1, refR1),
           output_temp(therm2, refR2)),
          end='', sep='', flush=True)
    #output_temp(therm3, refR3)
    time.sleep(2)

